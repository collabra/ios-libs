Install depot_tools and add to path per http://www.webrtc.org/native-code/development/prerequisite-sw

Create a working directory 'webrtc'

    export GYP_DEFINES="OS=ios"
    fetch webrtc_ios
    cd src 
    git checkout branch-heads/46
    gclient sync
    ./talk/build/build_ios_libs.sh
    pushd out_ios_libs
    ../chromium/src/third_party/libvpx/source/libvpx/build/make/iosbuild.sh
    mkdir include
    mkdir lib
    cp -r VPX.framework/Headers/vpx/ ./include
    mv fat/libvpx.a ./lib
    strip -S -X ./lib/libvpx.a
    zip -r libvpx.zip include/ lib/
    rm -r include
    rm -r lib
    rm -r VPX.framework
    libtool -static -v -o libWebRTC.a fat/*.a
    strip -S -X libWebRTC.a
    mkdir include
    mkdir lib
    mv libWebRTC.a ./lib/
    cp -r ../talk/app/webrtc/objc/public/*.h ./include
    zip -r webrtc-ios.zip include/ lib/
    rm -r include
    rm -r lib